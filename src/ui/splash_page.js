import React, {useState, useEffect} from 'react';
import { Button, Text, View } from 'react-native';

export default SplashPage = ({navigation}) =>{
    return (
        <>
            <View>
                <Text>Halmaan Splash</Text>
                <Button 
                    title='menuju halaman login'
                    onPress={()=> navigation.navigate("Login")}
                />
            </View>
        </>
    )
}