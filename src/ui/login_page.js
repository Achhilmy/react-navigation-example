import React, {useState, useEffecy} from 'react';
import { Button, Text, View } from 'react-native';

export default LoginPage =({navigation})=>{
    return (
        <>
            <View>

                <Button
                    title='menuju halaman Bottom Tab'
                    onPress={()=> navigation.navigate("MyBottomTab")}
                />
                 <Button
                    title='menuju halaman Drawer'
                    onPress={()=> navigation.navigate("MyDrawer")}
                />
            </View>
        </>
    )
}