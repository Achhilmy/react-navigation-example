import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, ScrollView , SafeAreaView, TextInput, TouchableOpacity, FlatList} from 'react-native';
import { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Routers from './src/routers/index.js';
import 'react-native-gesture-handler';

export default function App() {
  return  (
   <NavigationContainer>
      <Routers/>
   </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  logo: {
    width: 66,
    height: 58,
  },
  text: {
    fontSize: 42,
  }, 
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10
  }, 
});
