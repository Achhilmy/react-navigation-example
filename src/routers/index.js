import login_page from "../ui/login_page";
import splash_page from "../ui/splash_page";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from "react";
import Account from "../ui/account_page";
import Settings from "../ui/setting_page";
import { createDrawerNavigator } from '@react-navigation/drawer';
import feed_page from "../ui/feed_page";
import article_page from "../ui/article_page";

const Drawer = createDrawerNavigator();
const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

export default Router = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Splash" component={splash_page} />
            <Stack.Screen name="Login" component={login_page} />
            <Stack.Screen name="MyBottomTab" component={MyTabs} />
            <Stack.Screen name="MyDrawer" component={MyDrawer} />
        </Stack.Navigator>
    );
}
  
function MyTabs() {
    return (
      <Tab.Navigator>
        <Tab.Screen name="Home" component={MyDrawer} />
        <Tab.Screen name="Account" component={Account} />
        <Tab.Screen name="Settings" component={Settings} />
      </Tab.Navigator>
    );
  }
  function MyDrawer() {
    return (
      <Drawer.Navigator>
        <Drawer.Screen name="Feed" component={feed_page} />
        <Drawer.Screen name="Article" component={article_page} />
      </Drawer.Navigator>
    );
  }